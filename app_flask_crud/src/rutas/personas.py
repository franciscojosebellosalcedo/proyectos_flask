from flask import Blueprint 
from flask import redirect 
from flask import url_for 
from flask import request 
from flask import render_template 
from db.persona_db import insertarPersona,obtenerPersonas,editarPersona,obtenerPersona,eliminarPersona


persona=Blueprint("personas",__name__)

@persona.route("/")
def index():
    listaPersona=obtenerPersonas()
    print(listaPersona)
    return render_template("index.html",lista=listaPersona)


@persona.route("/agregar")
def agregar():
    return render_template("agregar.html")

@persona.route("/guardar",methods=["POST"])
def guardarPersona():
    if(request.method=="POST"):
        nombres=request.form.get("nombres")
        apellidos=request.form.get("apellidos")
        email=request.form.get("email")
        edad=request.form.get("edad")
        documento=request.form.get("documento")
        for persona in obtenerPersonas():
            if(documento==persona[4]):
                print("Ya existe")
            else:
                insertarPersona(nombres,apellidos,email,edad,documento)
    return redirect(url_for("personas.index"))

@persona.route("/editar/<documento>")
def editar(documento):
    print("documento: ",documento)
    print(obtenerPersona(documento))
    return render_template("editar.html",persona=obtenerPersona(documento))

@persona.route("/actualizar/<documento>",methods=["POST"])
def actualizar(documento):
    nombresNuevos=request.form.get("nombresNuevos")
    apellidosNuevos=request.form.get("apellidosNuevos")
    emailNuevo=request.form.get("emailNuevo")
    edadNueva=request.form.get("edadNueva")
    editarPersona(documento,nombresNuevos,apellidosNuevos,emailNuevo,edadNueva)
   
    return redirect(url_for("personas.index"))

@persona.route("/eliminar/<documento>")
def eliminar(documento):
    eliminarPersona(documento)
    return redirect(url_for("personas.index"))

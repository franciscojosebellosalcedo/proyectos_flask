from db.conexion import conexion

def insertarPersona(nombres,apellidos,email,edad,documento):
    con=conexion()
    cursor=con.cursor()
    cursor.execute("insert into personas (nombres,apellidos,email,edad,documento)values (%s,%s,%s,%s,%s)",(nombres,apellidos,email,edad,documento))
    con.commit()
    con.close()
    print("persona agregada")

def eliminarPersona(documento):
    con=conexion()
    cursor=con.cursor()
    cursor.execute("delete from personas where documento=%s",(documento))
    con.commit()
    con.close()
    print("persona eliminar")

def editarPersona(documento,nombresNuevo,apellidosNuevos,emailNuevo,edadNueva):
    con=conexion()
    cursor=con.cursor()
    cursor.execute(
        """update personas set nombres=%s,apellidos=%s,email=%s,edad=%s
        where documento=%s"""
    ,(nombresNuevo,apellidosNuevos,emailNuevo,edadNueva,documento))
    con.commit()
    con.close()
    print("persona editada")


def obtenerPersonas():
    con=conexion()
    cursor=con.cursor()
    cursor.execute("select * from personas")
    personas=cursor.fetchall()
    con.commit()
    con.close()
    return personas
    
def obtenerPersona(documento):
    con=conexion()
    cursor=con.cursor()
    cursor.execute("select * from personas where documento=%s",(documento))
    persona=cursor.fetchone()
   
    con.commit()
    con.close()
    return persona
